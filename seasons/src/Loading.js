import './Loading.css';
import React from 'react';

const Loading = (props) => {

  return (
    <div className="container-fluid overlay h-100">
      <div className="spinner-border" role="status">
        <span className="sr-only">Loading</span>
      </div>
      <p className="loading-text">{props.message}</p>
    </div>
  );
};

/**
 * Default prop values
 */
Loading.defaultProps = {
  message: 'Loading...'
};

export default Loading;