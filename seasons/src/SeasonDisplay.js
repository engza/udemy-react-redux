import './SeasonDisplay.css';
import React from 'react';

const seasonConfig = {
  summer: {
    seasonText: 'Let\'s hit the beach!',
    iconName: 'sun',
    iconColor: 'text-danger'
  },
  winter: {
    seasonText: 'Burr, it is chilly!',
    iconName: 'snowflake',
    iconColor: 'text-info'
  }
}

const getSeason = ( lat, month ) => {
  if ( month > 2 && month < 9 ) {
    return lat > 0 ? 'summer' : 'winter';
  } else {
    return lat > 0 ? 'winter' : 'summer';
  }
}

const SeasonDisplay = (props) => {

  const season = getSeason( props.lat, new Date().getMonth() );

  const { seasonText, iconName, iconColor } = seasonConfig[season];

  return (
    <div className={ `season-display ${season} py-4` }>
      <div className="sr-only">Season: { season }</div>
      <div className="sr-only">Google Maps:
        <a href={'https://www.google.com/maps/search/'+props.lat+','+props.lon+'/'} target="_blank" rel="noopener noreferrer"> { props.lat }, { props.lon }</a>
      </div>
      <div>
        <i className={ `fa fa-${iconName}-o ${iconColor} icon-left` } aria-hidden="true"></i>
        <h1>{ seasonText }</h1>
        <i className={ `fa fa-${iconName}-o ${iconColor} icon-right` } aria-hidden="true"></i>
      </div>
    </div>
  );
};

export default SeasonDisplay;