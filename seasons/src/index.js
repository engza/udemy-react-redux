import React from 'react';
import ReactDOM from 'react-dom';
import Loading from './Loading';
import SeasonDisplay from './SeasonDisplay';

class App extends React.Component {

  /**
   * Initialize state object without the constructor method
   */
  state = {
    lat: null,
    lon: null,
    errorMessage: ''
  };

  /**
   * ** componentDidMount **
   * Good place to do a one-time setup
   *    * network request to an API
   *    * geolocation call
   */
  componentDidMount() {

    // Prompts user to share their location
    window.navigator.geolocation.getCurrentPosition(
      // if success, output to console.log
      position => {
        // in order to update our state object, we call setState
        this.setState({ 
          lat: position.coords.latitude,
          lon: position.coords.longitude,
        });
        // console.log(position);
      },
      err => {
        this.setState({
          errorMessage: err.message
        });
        // console.log(err);
      }
    );

  }

  /**
   * Helper function
   * handles conditional logic
   */
  renderContent() {

    /**
     * If has errorMessage value but no latitude value
     */
    if ( this.state.errorMessage && !this.state.lat ) {
      return (
        <div className="container my-4">
          <div>Error: { this.state.errorMessage }</div>
        </div>
      );
    }

    /**
     * If has latitude value but no errorMessage value
     */
    if ( !this.state.errorMessage && this.state.lat ) {
      return <SeasonDisplay lat={ this.state.lat } lon={ this.state.lon } />;
    }

    /**
     * If has no latitude or errorMessage values
     */
    return <Loading message="Please accept location request." />;

  }

  /**
   * ** render **
   * required by React
   * not optional
   * avoid doing anything besides returning JSX
   */ 
  render () {

    return (
      <div 
        className='border border-danger'
        style={{height:'100vh',width:'100vw'}}
        /**
         * How to add !important to inline react styles
         * @link https://joshtronic.com/2018/03/22/how-to-important-inline-styles-in-react/
         */
        ref={(el) => {
          el&&el.style.setProperty('border-width', '1rem', 'important')
        }}
      >
        {this.renderContent()}
      </div>
    );

  }

}

ReactDOM.render(
  <App />,
  document.querySelector('#root')
);