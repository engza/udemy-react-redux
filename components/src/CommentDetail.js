// Import the React and ReactDOM libraries
import React from 'react';

const CommentDetail = props => {
  return (
    <li className="media my-4">
      <img src={props.avatar} className="mr-3 rounded" alt={props.author} />
      <div className="media-body">
        <div className="row">
          <div className="col-12">
            <div className="h5 mt-0 mb-1 float-left">
              <a href={'/?user='+props.author}>{props.author}</a>
            </div>
            <div className="h6 mt-1 mb-1 ml-2 text-muted float-left">{props.time}</div>
          </div>
        </div>
        <p>{props.comment}</p>
      </div>
    </li>
  );
};

export default CommentDetail;