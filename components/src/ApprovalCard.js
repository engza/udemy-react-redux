// Import the React and ReactDOM libraries
import React from 'react';

const ApprovalCard = props => {
  return (
    <div className="card my-4">
      <div className="card-body">{props.children}</div>
      <div className="row card-footer justify-content-center mx-0 bg-white">
        <div className="btn-group" role="group">
          <button type="button" className="btn btn-outline-success">Approve</button>
          <button type="button" className="btn btn-outline-danger">Deny</button>
        </div>
      </div>
    </div>
  );
};

export default ApprovalCard;