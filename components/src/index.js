// Import the React and ReactDOM libraries
import React from 'react';
import ReactDOM from 'react-dom';
import faker from 'faker';
import CommentDetail from './CommentDetail';
import ApprovalCard from './ApprovalCard';
import 'bootstrap/dist/css/bootstrap.min.css';

function time() {
  const dayArray = ['Sun','Mon','Tue','Wed','Thu','Fri','Sat'];
  const monthArray = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
  const date = new Date(faker.date.past());
  var hours = date.getHours();
  var minutes = date.getMinutes();
  const ampm = hours >= 12 ? 'PM' : 'AM';
  hours = hours % 12;
  hours = hours ? hours : 12; // the hour '0' should be '12'
  minutes = minutes < 10 ? '0'+minutes : minutes;
  const dayName = date.getDay();
  const moName = date.getMonth();
  const dayNo = date.getDate();
  const year = date.getFullYear();
  const strTime = dayArray[dayName] + ', ' + monthArray[moName] + ' ' + dayNo + ', ' + year + ' at ' + hours + ':' + minutes + ' ' + ampm;
  return strTime;

  // Sat, Mar 23, 2019 at 5:45 PM
}

// Create a React component
const App = () => {
  return (
    <div className="container">
      <ul className="list-unstyled">
        <ApprovalCard>
          <CommentDetail 
            author={faker.internet.userName()} 
            time={time()} 
            avatar={faker.internet.avatar()} 
            comment={faker.lorem.paragraph()} 
          />
        </ApprovalCard>
        <ApprovalCard>
          <CommentDetail 
            author={faker.internet.userName()} 
            time={time()} 
            avatar={faker.internet.avatar()} 
            comment={faker.lorem.paragraph()} 
          />
        </ApprovalCard>
        <ApprovalCard>
          <CommentDetail 
            author={faker.internet.userName()} 
            time={time()} 
            avatar={faker.internet.avatar()} 
            comment={faker.lorem.paragraph()} 
          />
        </ApprovalCard>
        <ApprovalCard>
          <CommentDetail 
            author={faker.internet.userName()} 
            time={time()} 
            avatar={faker.internet.avatar()} 
            comment={faker.lorem.paragraph()} 
          />
        </ApprovalCard>
      </ul>
    </div>
  );
};

// Show React component on the screen
ReactDOM.render(
  <App />,
  document.querySelector('#root')
);