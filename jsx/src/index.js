// Import the React and ReactDOM libraries
import React from 'react';
import ReactDOM from 'react-dom';
import 'bootstrap/dist/css/bootstrap.min.css';

// Create a React component
const App = () => {

  const buttonText = { text: 'Click Me' };
  const labelText = 'Enter Name:';

  return (
    <div>
      <div className="form-group">
        <label className="label" htmlFor="name">{labelText}</label>
        <input type="text" className="form-control" id="name" />
      </div>
      <button className="btn btn-primary">{buttonText.text}</button>
    </div>
  );
};

// Show React component on the screen
ReactDOM.render(
  <App />,
  document.querySelector('#root')
);