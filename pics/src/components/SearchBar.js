import React from 'react';

class SearchBar extends React.Component {

  state = {
    term: ''
  };

  onFormSubmit = e => {
    e.preventDefault();

    // passes the "term" back up to the parent
    this.props.onSubmit(this.state.term);
  };

  render() {
    return (
      <form onSubmit={this.onFormSubmit}>
        <div className='card shadow-sm'>
          <div className='card-body'>
            <div className="form-group">
              <label htmlFor='search'>Image Search</label>
              <input
                type='text'
                className='form-control'
                id='search'
                value={this.state.term}
                /**
                 * 'onChange' Notes:
                 *    - if you add parenthesis after the function, it only renders on load
                 *    - if you leave the parenthesis off, you are passing the function to
                 *      the element and it can call the function at some point in the future
                 */
                onChange={ e=>this.setState({ term:e.target.value }) }
              />
            </div>
          </div>
        </div>
      </form>
    )
  }
}

export default SearchBar;