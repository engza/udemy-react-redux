import axios from 'axios';

export default axios.create({
  baseURL: 'https://api.unsplash.com',
  headers: {
    Authorization: 'Client-ID 2b51862920cd2add6977d81b6b9faecd4ef9f579b7552f59752b46d8aca5f8fc'
  }
});