## Modern React with Redux

### Links
- [Modern React with Redux | Udemy](https://www.udemy.com/react-redux/learn/v4/overview)
- [Instructor's Course Repository | GitHub](https://github.com/StephenGrider/redux-code)

### Projects
- [JSX](./jsx)
- [Components](./components)
- [Seasons](./seasons)
- [Pics](./pics)